//
// Created by michael on 04.09.2020.
//

#include "Library.h"


Library::Library(int booksAmount, int readersAmount) :
    _booksAmount(booksAmount),
    _readersAmount(readersAmount){

    if(_booksAmount > 0){
        _books = new Book*[_booksAmount];
    }else{
        throw LibraryClassException("library can`t exist without books");
    }

    if(_readersAmount > 0){
        _readers = new Reader*[_readersAmount];
    }else{
        throw LibraryClassException("library can`t exist without readers");
    }

}

int Library::GiveOutBook(int book_id, int readers_id) {
    if(book_id <= _booksAmount && _books[book_id - 1]->IsPresent()){
        bool found_reader = false;
        int readers_index = 0;
        while(!found_reader && readers_index < _readersAmount){
            if(_readers[readers_index]->getId() == readers_id){
                found_reader = true;
            }else{
                readers_index++;
            }
        }

        if(found_reader && !_readers[readers_index]->hasBook()){
            _books[book_id - 1]->SetStatusTaken();
            _readers[readers_index]->takesBook(book_id);
            return 1;
        }else{
            return 0;
        }

    }else{
        return 0;
    }
}

unsigned int Library::RetrieveBook(unsigned int readers_id) {
    bool found_reader = false;
    int readers_index = 0;
    while(readers_index < _readersAmount){
        if(_readers[readers_index]->getId() == readers_id){
            found_reader = true;
            break;
        }else{
            readers_index++;
        }
    }

    if(found_reader && _readers[readers_index]->hasBook()){
        unsigned int book_id = _readers[readers_index]->returnsBook();
        _books[book_id-1]->SetStatusPresent();
        return book_id;
    }else{
        return 0;
    }
}

int Library::GetBookStatus(unsigned int book_id) {
    if(book_id < _booksAmount) {
        if (_books[book_id - 1]->IsPresent()) {
            return 0;
        } else {
            bool found_reader = false;
            int readers_index = 0;
            while (!found_reader) {
                if (_readers[readers_index]->getBookId() == book_id) {
                    found_reader = true;
                } else {
                    readers_index++;
                }
            }
            return _readers[readers_index]->getId();
        }
    }else{
        return -1;
    }
}


Library::~Library() {
    CleanUp();
}

void Library::CleanUp() {
    if(_books != nullptr){
        for(int counter = 0; counter < _booksAmount; counter++){
            delete _books[counter];
        }
        delete [] _books;
    }
    if(_readers != nullptr){
        for(int counter = 0; counter < _readersAmount; counter++){
            delete _readers[counter];
        }
        delete [] _readers;
    }
}

Book **Library::GetBooks() const {
    return _books;
}

int Library::GetBooksAmount() const {
    return _booksAmount;
}

Reader **Library::GetReaders() const {
    return _readers;
}

int Library::GetReadersAmount() const {
    return _readersAmount;
}

void Library::AddBook(Book *book) {
    if (_addedBooksCounter < _booksAmount){
        _books[_addedBooksCounter] = book;
        _addedBooksCounter++;
    }else{
        throw LibraryClassException("tried to add extra books");
    }
}

void Library::AddReader(Reader *reader) {
    if(_addedReadersCounter < _readersAmount){
        _readers[_addedReadersCounter] = reader;
        _addedReadersCounter++;
    }else{
        throw LibraryClassException("tried to add extra readers");
    }
}


