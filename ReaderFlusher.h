//
// Created by michael on 08.09.2020.
//

#ifndef LAB1_OOP_READERFLUSHER_H
#define LAB1_OOP_READERFLUSHER_H

#include <ostream>
#include "Reader.h"

class ReaderFlusher {
public:
    ReaderFlusher(Reader** readersStorage, int readersAmount);
    void Flush(std::ostream &out);
    ~ReaderFlusher();
private:
    Reader** _readersStorage;
    int _readersAmount;
    void CleanUp();
};


#endif LAB1_OOP_READERFLUSHER_H
