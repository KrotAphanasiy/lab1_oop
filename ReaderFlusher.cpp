//
// Created by michael on 08.09.2020.
//

#include "ReaderFlusher.h"
#include <ostream>

ReaderFlusher::ReaderFlusher(Reader **readersStorage, int readersAmount):
    _readersStorage(readersStorage),
    _readersAmount(readersAmount){

}

void ReaderFlusher::Flush(std::ostream &out) {
    out << "@READERSAMOUNT\n" << _readersAmount << std::endl;
    for (int counter = 0; counter < _readersAmount; counter++) {
        out << "@READER" << std::endl;
        out << "_ID: " << _readersStorage[counter]->getId() << std::endl;
        if (_readersStorage[counter]->hasBook()) {
            out << "_BOOK_ID: " << _readersStorage[counter]->getBookId() << std::endl << std::endl;
        } else {
            out << std::endl;
        }
    }
}

void ReaderFlusher::CleanUp() {
    _readersStorage = nullptr;
}

ReaderFlusher::~ReaderFlusher() {
    CleanUp();
}
