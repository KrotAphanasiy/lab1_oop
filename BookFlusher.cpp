//
// Created by michael on 08.09.2020.
//

#include "BookFlusher.h"
#include <ostream>

BookFlusher::BookFlusher(Book** booksStorage, int booksAmount):
    _booksStorage(booksStorage),
    _booksAmount(booksAmount){
}

void BookFlusher::Flush(std::ostream& out) {

    out << "@BOOKSAMOUNT\n" << _booksAmount << std::endl << std::endl;
    for(int counter = 0; counter < _booksAmount; counter++) {
        out << "@BOOK" << std::endl;
        out << "_Title: " << _booksStorage[counter]->GetTitle() << std::endl;
        out << "_Authors: " << _booksStorage[counter]->GetAuthor() << std::endl;
        out << "_Publisher: " << _booksStorage[counter]->GetPublisher() << std::endl;
        out << "_Pub_year: " << _booksStorage[counter]->GetPubYear() << std::endl;
        out << "_Present: " << _booksStorage[counter]->IsPresent() << std::endl;

        if (counter < _booksAmount - 1) {
            out << std::endl;
        }
    }
}

void BookFlusher::CleanUp() {
    _booksStorage = nullptr;
}

BookFlusher::~BookFlusher() {
    CleanUp();
}


