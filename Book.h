//
// Created by michael on 04.09.2020.
//

#ifndef LAB1_OOP_BOOK_H
#define LAB1_OOP_BOOK_H

#include <string>

class Book {
public:
    Book() = default;
    Book(const std::string author, const std::string title, const std::string publisher,
         int pub_year, unsigned int id, bool present);
    //установка обязательной информации
    //установка статуса "присутсвует"
    void SetStatusPresent();
    //установка статуса "отсутсвует"
    void SetStatusTaken();
    //присутсвует ли
    bool IsPresent() const;

    //получение необходимой для функционирования библиотеки информации

    std::string GetTitle() const;
    std::string GetAuthor() const;
    std::string GetPublisher() const;
    unsigned int GetPubYear() const;
    unsigned int GetID() const;

private:
    std::string _author = "";
    std::string _title = "";
    std::string _publisher = "";
    unsigned int _id = 0;
    unsigned int _pub_year = 0;
    bool _present = false;
};

#endif LAB1_OOP_BOOK_H
