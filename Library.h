//
// Created by michael on 04.09.2020.
//

#ifndef LAB1_OOP_LIBRARY_H
#define LAB1_OOP_LIBRARY_H

#include "ReaderParser.h"
#include "BookParser.h"

class LibraryClassException{
public:
    LibraryClassException(std::string error): _error(error){}
    const char* getError() const{
        return _error.c_str();
    }
private:
    std::string _error;
};

class Library {
public:
    Library(int booksAmount, int readersAmount);
    ~Library();

    //выдача книги
    int GiveOutBook(int book_id, int readers_id);
    //возвращение книги
    unsigned int RetrieveBook(unsigned int readers_id);
    //получение информации о книге
    int GetBookStatus(unsigned int book_id);

    void AddBook(Book* book);
    Book** GetBooks() const;
    int GetBooksAmount() const;

    void AddReader(Reader* reader);
    Reader** GetReaders() const;
    int GetReadersAmount() const;

private:
    int _booksAmount = 0;
    int _addedBooksCounter = 0;
    int _readersAmount = 0;
    int _addedReadersCounter = 0;
    Book** _books = nullptr;
    Reader** _readers = nullptr;
    //очистка памяти
    void CleanUp();
};


#endif LAB1_OOP_LIBRARY_H
