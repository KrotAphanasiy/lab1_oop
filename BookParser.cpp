//
// Created by michael on 08.09.2020.
//

#include "BookParser.h"
#include <fstream>


//убираем пробелы слева
void BookParser::ltrim(std::string& s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

//убираем пробелы справа
void BookParser::rtrim(std::string& s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

//убираем пробелы слева и справа
void BookParser::trim(std::string& s) {
    ltrim(s);
    rtrim(s);
}

BookParser::BookParser(const std::string &fileToParsePath) :
    _fileToParsePath(fileToParsePath){
}


Book* BookParser::ParseBookFromFile() {

    std::ifstream booksFileStream(_fileToParsePath);
    if(!booksFileStream.is_open()){
        throw BookParserException("some files doesn`t exist");
    }

    booksFileStream.seekg(_posInBooksFile);

    std::ifstream& booksFileStreamRef = booksFileStream;

    Book* book = nullptr;
    while(!booksFileStream.eof() && !_bookData.fullInfo){
        ParseBookData(booksFileStreamRef);

        if (!_bookData.bookTitle.empty() && !_bookData.bookAuthors.empty() && !_bookData.bookPublisher.empty() && _bookData.bookPubYear != 0 && _bookData.presenceSet) {
            _bookData.fullInfo = true;
        }

        if (_bookData.fullInfo && _bookData.bookFound) {
            book = new Book(_bookData.bookAuthors, _bookData.bookTitle, _bookData.bookPublisher,
                _bookData.bookPubYear, _booksCounter + 1, _bookData.bookPresent);
            _booksCounter++;
            CleanBookData();
        }
    }

    if(_bookData.bookFound && booksFileStream.eof()) {
        return nullptr;
    }else if (!_bookData.fullInfo && _bookData.bookFound) {
        throw BookParserException("wrong books info format");
    }


    if(booksFileStream.eof() && _booksCounter < _booksAmount){
        throw BookParserException("Incomplete info about books (check @BOOKSAMOUNT)");
    }


    _bookData.fullInfo = false;
    _posInBooksFile = booksFileStream.tellg();
    return book;
}

int BookParser::GetBooksAmount() const
{
    return _booksAmount;
}

void BookParser::ParseBookData(std::ifstream& booksFileStream) {
    booksFileStream >> _bookData.fieldName;
    if (_bookData.fieldName == "@BOOK") {
        _bookData.bookFound = true;
        _bookData.fullInfo = false;
        return;
    }
    else if (_bookData.fieldName == "_Title:") {
        if (_bookData.bookTitle.empty()) {
            getline(booksFileStream, _bookData.bookTitle);
            trim(_bookData.bookTitle);
        }
        else {
            throw BookParserException("wrong books info format");
        }
    }
    else if (_bookData.fieldName == "_Authors:") {
        if (_bookData.bookAuthors.empty()) {
            getline(booksFileStream, _bookData.bookAuthors);
            trim(_bookData.bookAuthors);
        }
        else {
            throw BookParserException("wrong books info format");
        }
    }
    else if (_bookData.fieldName == "_Publisher:") {
        if (_bookData.bookPublisher.empty()) {
            getline(booksFileStream, _bookData.bookPublisher);
            trim(_bookData.bookPublisher);
        }
        else {
            throw BookParserException("wrong books info format");
        }
    }
    else if (_bookData.fieldName == "_Pub_year:") {
        if (_bookData.bookPubYear == 0) {
            booksFileStream >> _bookData.bookPubYear;
        }
        else {
            throw BookParserException("wrong books info format");
        }
    }
    else if (_bookData.fieldName == "_Present:") {
        if (!_bookData.presenceSet) {
            booksFileStream >> _bookData.bookPresent;
            _bookData.presenceSet = true;
        }
        else {
            throw BookParserException("wrong books info format");
        }
    }

}



int BookParser::ParseBooksAmount() {

    std::ifstream booksFileStream(_fileToParsePath);
    booksFileStream.seekg(_posInBooksFile);
    int booksAmount = 0;

    bool reachedBook = false;

    while (booksFileStream && !reachedBook) {
        std::string temp;
        getline(booksFileStream, temp);
        if (temp == "@BOOKSAMOUNT"){
            booksFileStream >> booksAmount;
            break;
        }
        if (temp == "@BOOK") {
            reachedBook = true;
        }
    }

    if(reachedBook){
        throw BookParserException("No info about books amount");
    }

    _posInBooksFile = booksFileStream.tellg();
    _booksAmount = booksAmount;

    return booksAmount;
}


void BookParser::CleanBookData() {
    _bookData.bookAuthors = "";
    _bookData.bookTitle = "";
    _bookData.bookPublisher = "";
    _bookData.bookPubYear = 0;
    _bookData.presenceSet = false;
    _bookData.fieldName = "";
}



