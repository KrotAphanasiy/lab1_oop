//
// Created by michael on 08.09.2020.
//

#include "ReaderParser.h"
#include <fstream>

ReaderParser::ReaderParser(const std::string &fileToParsePath):
    _fileToParsePath(fileToParsePath){
}

Reader* ReaderParser::ParseReaderFromFile() {
    std::ifstream readersFileStream(_fileToParsePath);
    if(!readersFileStream.is_open()){
        throw ReaderParserException("some files doesn`t exist");
    }
    if(_posInReadersFile >= 0) {
        readersFileStream.seekg(_posInReadersFile);
    }else{
        return nullptr;
    }
    std::ifstream& readersFileStreamRef = readersFileStream;


    Reader* reader = nullptr;
    while (!readersFileStream.eof() && !_readerData.reachedNext) {
         ParseReaderData(readersFileStreamRef);

        if (_readerData.reachedNext) {
            if (_readerData.hasBook) {
                reader = new Reader(_readerData.id, _readerData.bookId);
            } else {
                reader = new Reader(_readerData.id);
            }
            CleanReaderData();
            _readersCounter++;
        }
    }
    if(readersFileStream.eof() && _readerData.readerFound){
        if(_readerData.id == 0){
            throw ReaderParserException("readers file storage unexpectedly exposed");
        }
        if (_readerData.hasBook) {
            reader = new Reader(_readerData.id, _readerData.bookId);
        } else {
            reader = new Reader(_readerData.id);
        }
        CleanReaderData();
        _readersCounter++;
    }

    if(!_readerData.readerFound && readersFileStream.eof()){
        return nullptr;
    }else if(readersFileStream.eof() && _readersCounter < _readersAmount){
        throw ReaderParserException("incomplete readers info");
    }

    if(readersFileStream.eof() && _readersCounter < _readersAmount){
        throw ReaderParserException("Incomplete info about readers (check @READERSAMOUNT)");
    }

    _readerData.readerFound = false;
    _readerData.reachedNext = false;
    _posInReadersFile = readersFileStream.tellg();
    return reader;
}

void ReaderParser::ParseReaderData(std::ifstream& readersFileStream) {
    readersFileStream >> _readerData.fieldName;

    if (_readerData.fieldName == "@READER") {
        if(!_readerData.readerFound){
            _readerData.readerFound = true;
        }else {
            _readerData.reachedNext = true;
            readersFileStream.seekg(_posInReadersFile);
            return;
        }
    } else if (_readerData.fieldName == "_ID:") {
        if (_readerData.id == 0) {
            readersFileStream >> _readerData.id;
            _posInReadersFile = readersFileStream.tellg();
        } else {
            throw ReaderParserException("wrong readers info format");
        }
    } else if (_readerData.fieldName == "_BOOK_ID:") {
        if (_readerData.bookId == 0) {
            readersFileStream >> _readerData.bookId;
            _readerData.hasBook = true;
            _posInReadersFile = readersFileStream.tellg();
        } else {
            throw ReaderParserException("wrong readers info format");
        }
    }
    _readerData.fieldName = "";
}

int ReaderParser::GetReadersAmount() const {
    return _readersAmount;
}

int ReaderParser::ParseReadersAmount() {

    std::ifstream readersFileStream(_fileToParsePath);
    readersFileStream.seekg(_posInReadersFile);

    int readersAmount = 0;

    bool reachedReader = false;

    while (readersFileStream && !reachedReader) {
        std::string temp;
        getline(readersFileStream, temp);
        if (temp == "@READERSAMOUNT"){
            readersFileStream >> readersAmount;
            break;
        }
        if (temp == "@READER") {
            reachedReader = true;
        }
    }

    if(reachedReader){
        throw ReaderParserException("No info about books amount");
    }

    _posInReadersFile =  readersFileStream.tellg();
    _readersAmount = readersAmount;

    return readersAmount;
}

void ReaderParser::CleanReaderData() {
    _readerData.bookId = 0;
    _readerData.id = 0;
    _readerData.hasBook = false;
    _readerData.fieldName = "";
}


