//
// Created by michael on 08.09.2020.
//

#ifndef LAB1_OOP_BOOKFLUSHER_H
#define LAB1_OOP_BOOKFLUSHER_H
#include "Book.h"

class BookFlusher{
public:
    BookFlusher(Book** booksStorage, int booksAmount);
    void Flush(std::ostream& out);
    ~BookFlusher();
private:
    Book** _booksStorage;
    int _booksAmount;
    void CleanUp();
};


#endif LAB1_OOP_BOOKFLUSHER_H
