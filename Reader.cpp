//
// Created by michael on 04.09.2020.
//

#include "Reader.h"

void Reader::takesBook(unsigned int book_id) {
    _bookId = book_id;
    _hasBook = true;
}

int Reader::returnsBook() {
    unsigned int to_return = _bookId;
    _bookId = 0;
    _hasBook = false;
    return to_return;
}

bool Reader::hasBook() {
    return _hasBook;
}

int Reader::getBookId() {
    return _bookId;
}

int Reader::getId() {
    return _id;
}

Reader::Reader(int id) : _id(id){

}

Reader::Reader(int id, int bookId):_id(id), _bookId(bookId), _hasBook(true) {

}




