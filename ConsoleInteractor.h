//
// Created by michael on 04.09.2020.
//

#ifndef LAB1_OOP_CONSOLEINTERACTOR_H
#define LAB1_OOP_CONSOLEINTERACTOR_H

#include "Library.h"
#include "BookFlusher.h"
#include "ReaderFlusher.h"

//взаимодейтсвие с консолью
class ConsoleInteractor {
public:

    //инициализация консольного интерфейса
    void init();

    ~ConsoleInteractor();
private:
    Library* _library = nullptr;
    BookParser* _bookParser = nullptr;
    ReaderParser* _readerParser = nullptr;
    BookFlusher* _bookFlusher = nullptr;
    ReaderFlusher* _readerFlusher = nullptr;
    //очистка памяти
    void CleanUp();
};


#endif LAB1_OOP_CONSOLEINTERACTOR_H
