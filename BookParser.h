//
// Created by michael on 08.09.2020.
//

#ifndef LAB1_OOP_BOOKPARSER_H
#define LAB1_OOP_BOOKPARSER_H


#include "Book.h"

class BookParserException{
public:
    BookParserException(std::string error): _error(error){}
    const char* getError() const{
        return _error.c_str();
    }
private:
    std::string _error;
};

struct BookData{
    std::string fieldName = "";
    std::string bookTitle = "";
    std::string bookAuthors = "";
    std::string bookPublisher = "";
    unsigned int bookPubYear = 0;
    bool bookPresent = false;
    bool presenceSet = false;
    bool fullInfo = false;
    bool bookFound = false;
};

class BookParser {
public:
    BookParser(const std::string& fileToParsePath);
    ~BookParser() = default;

    Book* ParseBookFromFile();
    int GetBooksAmount() const;
    int ParseBooksAmount();

private:
    std::string _fileToParsePath = "";
    int _booksAmount = 0;
    int _booksCounter = 0;
    int _posInBooksFile = 0;
    void trim(std::string& s);
    void ltrim(std::string& s);
    void rtrim(std::string& s);
    void ParseBookData(std::ifstream& stream);
    void CleanBookData();
    BookData _bookData;
};


#endif LAB1_OOP_BOOKPARSER_H
