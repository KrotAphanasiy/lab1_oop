//
// Created by michael on 04.09.2020.
//

#include "ConsoleInteractor.h"
#include <iostream>
#include <fstream>

void ConsoleInteractor::init() {
    std::string booksPath;
    std::string readersPath;

    std::cout << "Имя файла с книгами: ";
    std::cin >> booksPath;

    std::cout << "Имя файла с читателями: ";
    std::cin >> readersPath;

    _bookParser = new BookParser(booksPath);
    _readerParser = new ReaderParser(readersPath);

    _library = new Library(_bookParser->ParseBooksAmount(), _readerParser->ParseReadersAmount());
    try {
        while (Book *book = _bookParser->ParseBookFromFile()) {
            _library->AddBook(book);
        }

        while (Reader *reader = _readerParser->ParseReaderFromFile()) {
            _library->AddReader(reader);
        }
    }catch(const BookParserException& exc1){
        std::cerr << exc1.getError();
    }catch(const ReaderParserException& exc2){
        std::cerr << exc2.getError();
    }

    _bookFlusher = new BookFlusher(_library->GetBooks(), _library->GetBooksAmount());
    _readerFlusher = new ReaderFlusher(_library->GetReaders(), _library->GetReadersAmount());

    std::cout << "\nBook List\n";
    _bookFlusher->Flush(std::cout);

    std::cout << "\nReaders Registered\n";
    _readerFlusher->Flush(std::cout);

    std::cout << "Actions:\n1 - Give book (args: book_id, reader_id)\n2 - Retrieve book (args: reader_id)\n3 - Display info\n4 - get book`s info (args: book_id)\n5 - exit\n\n";

    //реализация примитивной системы команд
    int action;
    while(true){
        std::cout << ">> ";
        std::cin >> action;
        
        int book_id, reader_id;
        if (action == 1) {

            std::cout << "args: ";
            std::cin >> book_id >> reader_id;
            if (_library->GiveOutBook(book_id, reader_id)) {
                std::cout << "Done!\n";
            }
            else {
                std::cout << "Book is not present, not found or reader already has a book!\n";
            }
           
        }
        else if (action == 2) {

            std::cout << "args: ";
            std::cin >> reader_id;
            if (_library->RetrieveBook(reader_id)) {
                std::cout << "Done!\n";
            }
            else {
                std::cout << "Reader doesn`t have a book or such reader doesn`t exist!\n";
            }
        }
        else if (action == 3) {
            std::cout << "\nActions:\n1 - Give book (args: book_id, reader_id)\n2 - Retrieve book (args: reader_id)\n3 - Display info\n4 - get book`s info (args: book_id)\n5 - exit\n\n";
            std::cout << "\nBook List\n";
            _bookFlusher->Flush(std::cout);

            std::cout << "\nReaders Registered\n";
            _readerFlusher->Flush(std::cout);
        }
        else if(action == 4) {
            std::cout << "args: ";
            std::cin >> book_id;
            unsigned int book_status = _library->GetBookStatus(book_id);
            if(book_status == -1){
                std::cout << "Book " << book_id << " doesn`t exist!" << std::endl;
            }
            else if (book_status) {
                std::cout << "Book " << book_id << " is given to reader " << book_status << std::endl;
            }
            else {
                std::cout << "Book " << book_id << " is present in the library\n";
            }
        }
        else if(action == 5){
            std::ofstream booksOut(booksPath);
            std::ofstream readersOut(readersPath);
            _bookFlusher->Flush(booksOut);
            _readerFlusher->Flush(readersOut);
            break;
        }

    }
}


void ConsoleInteractor::CleanUp() {
    if(_library != nullptr){
        delete _library;
        _library = nullptr;
    }
    if(_bookFlusher != nullptr){
        delete _bookFlusher;
    }
    if(_readerFlusher != nullptr){
        delete _readerFlusher;
    }
    if(_readerParser != nullptr){
        delete _readerParser;
    }

    if(_bookParser != nullptr){
        delete _bookParser;
    }

}

ConsoleInteractor::~ConsoleInteractor() {
    CleanUp();
}
