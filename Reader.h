//
// Created by michael on 04.09.2020.
//

#ifndef LAB1_OOP_READER_H
#define LAB1_OOP_READER_H

class Reader {
public:
    Reader() = default;
    Reader(int id);
    Reader(int id, int bookId);
    ~Reader() = default;

    //получение книги
    void takesBook(unsigned int book_id);
    //возвращение книги
    int returnsBook();
    //имеет ли книгу
    bool hasBook();

    int getBookId();
    int getId();

private:
    int _id = 0;
    int _bookId = 0;
    bool _hasBook = false;
};


#endif LAB1_OOP_READER_H
