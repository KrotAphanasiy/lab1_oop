//
// Created by michael on 04.09.2020.
//

#include "Book.h"

void Book::SetStatusPresent() {
    _present = true;
}

void Book::SetStatusTaken() {
    _present = false;
}

bool Book::IsPresent() const {
    return _present;
}

std::string Book::GetTitle() const {
    return _title;
}

std::string Book::GetAuthor() const {
    return _author;
}

std::string Book::GetPublisher() const {
    return _publisher;
}

unsigned int Book::GetPubYear() const {
    return _pub_year;
}

unsigned int Book::GetID() const {
    return _id;
}

Book::Book(const std::string author, const std::string title, const std::string publisher, int pub_year,
           unsigned int id, bool present):
           _author(author),
           _title(title),
           _publisher(publisher),
           _pub_year(pub_year),
           _present(present),
           _id(id){

}




