//
// Created by michael on 08.09.2020.
//

#ifndef LAB1_OOP_READERPARSER_H
#define LAB1_OOP_READERPARSER_H

#include "Reader.h"
#include <string>

class ReaderParserException{
public:
    ReaderParserException(std::string error): _error(error){}
    const char* getError() const{
        return _error.c_str();
    }
private:
    std::string _error;
};

struct ReaderData{
    std::string fieldName = "";
    int id = 0;
    int bookId = 0;
    bool hasBook = false;
    bool readerFound = false;
    bool reachedNext = false;
};

class ReaderParser {
public:
    ReaderParser(const std::string& fileToParsePath);
    ~ReaderParser() = default;

    Reader* ParseReaderFromFile();
    int ParseReadersAmount();
    int GetReadersAmount() const;

private:
    ReaderData _readerData;
    std::string _fileToParsePath = "";
    int _posInReadersFile = 0;
    int _readersAmount = 0;
    int _readersCounter = 0;
    void ParseReaderData(std::ifstream& readersFileStream);
    void CleanReaderData();

};


#endif LAB1_OOP_READERPARSER_H
